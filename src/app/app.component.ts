import { Component, ViewChild } from '@angular/core';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';
import { Http, URLSearchParams } from '@angular/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  accessToken: any;
  loginStatus: boolean;
  uploadForm: FormGroup;
  //url : any;
 // params : any;

  constructor(private fb: FacebookService,
    private http: Http ,
    public formbuilder: FormBuilder
  ) {

    this.loginStatus = false;
    //this.UploadImage();    

    this.uploadForm = this.formbuilder.group({      
      url: ['', Validators.required],    
        
    });

    console.log('Initializing Facebook');

    fb.init({
      appId: '165288270864212',
      cookie: false,
      xfbml: true,
      version: 'v2.11'
    });

  }

  /**
   * Login with additional permissions/options
   */
  loginWithOptions() {

    const loginOptions: LoginOptions = {
      enable_profile_selector: true,
      return_scopes: true,
      scope: 'user_friends, email, publish_actions, manage_pages, pages_show_list, publish_pages, public_profile'
    };

    this.fb.login(loginOptions)
      .then((res: LoginResponse) => {
        this.accessToken = res.authResponse.accessToken;
        this.loginStatus = true;
        console.log('Logged in', res);
      })
      .catch(this.handleError);

  }

  /**
   * This is a convenience method for the sake of this example project.
   * Do not use this in production, it's better to handle errors separately.
   * @param error
   */
  private handleError(error) {
    console.error('Error processing action', error);
  }  

  UploadImage() {   
    //var reader = new FileReader();

   // reader.readAsDataURL(this.uploadForm.value);

    //let Form = JSON.stringify(this.uploadForm.value.toURL());

   // Form = this.url.toURL();

    let params = {
      url: JSON.stringify(this.uploadForm.value.toURL()),
      published: true,
      access_token: this.accessToken
    };
      //'http://www.friendshipday.quotesms.com/images/fs-images/friendship-images.jpg',    

    this.fb.api('/543337362691526/photos', 'post', params).then().catch(this.handleError);
  }

  logout() {
    this.fb.logout()
    this.loginStatus = false;

  }
}
